package com.homework3.task2;

import java.util.*;

public class Converter {

    private List<String> queue = new ArrayList<>();
    private Stack<String> stack = new Stack<>();
    private String[] infix;
    private Map<String, Integer> mathSigns = new HashMap<>() {{
        put("+", 10);
        put("-", 10);
        put("*", 20);
        put("/", 20);
    }};

    public Converter(String[] values) {
        this.infix = values;
    }

    private boolean isNumber(String str) {
        if (str == null || str.isEmpty()) return false;
        for (int i = 0; i < str.length(); i++) {
            if (!Character.isDigit(str.charAt(i))) return false;
        }
        return true;
    }

    public String convertToPostfixNotation() {
        for (String i : infix) {
            if (isNumber(i)) {
                queue.add(i);
                continue;
            }
            if (mathSigns.containsKey(i)) {
                if (stack.isEmpty() || stack.peek().equals("(")) {
                    stack.push(i);
                    continue;
                }
                if (mathSigns.get(i) <= mathSigns.get(stack.peek())) {
                    while (!stack.isEmpty() && !stack.peek().equals("(") && mathSigns.get(i) <= mathSigns.get(stack.peek())) {
                        queue.add(stack.pop());
                    }
                }
                stack.push(i);
                continue;
            }
            if (i.equals("(")) {
                stack.push(i);
                continue;
            }
            if (i.equals(")")) {
                while (!stack.peek().equals("(")) {
                    if (stack.isEmpty()) {
                        throw new IllegalArgumentException("Error in writing expression");
                    }
                    queue.add(stack.pop());
                }
                stack.pop();
                continue;
            }
        }
        while (!stack.isEmpty()) {
            queue.add(stack.pop());
        }

        return String.join(" ", queue);
    }
}
