package com.homework3.task2;

public class Main {

    public static void testMethodConverter(String infixNotation) {
        try {
            System.out.println("Инфиксное выражение: " + infixNotation);
            String[] values = infixNotation.split(" ");
            Converter converter = new Converter(values);
            System.out.println("Постфиксное выражение: " + converter.convertToPostfixNotation());
        } catch (RuntimeException exception) {
            System.out.println(exception.getMessage());
        }
    }

    public static void main(String[] args) {
        testMethodConverter("5 * 6 + ( 2 - 9 )");
        System.out.println("========================");
        testMethodConverter("5 * 8 * ( 2 + 9 ) + ( 7 - 5 + 8 - 9 * ( 5 * 5 ) + 5 )");
        System.out.println("========================");
        testMethodConverter("( 5 * 6 ) / ( 2 * 9 )");
    }
}
