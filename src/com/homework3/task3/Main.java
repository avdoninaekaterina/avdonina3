package com.homework3.task3;

import java.util.LinkedHashMap;

public class Main {

    public static void testMethodCounter(String text) {
        try {
            System.out.println("Выражение: " + text);
            Counter counter = new Counter(text);
            LinkedHashMap<Character, Integer> sortedCharacters = counter.getCountInDescendingOrder();
            System.out.println("Результат: ");
            sortedCharacters.entrySet().forEach((p) -> System.out.print("'" + p.getKey() + "' = " + p.getValue() + ", "));
            System.out.println("");
        } catch (RuntimeException exception) {
            System.out.println(exception.getMessage());
        }
    }

    public static void main(String[] args) {
        testMethodCounter("В чащах юга жил бы цитрус?");//русский
        testMethodCounter("Zəfər, jaketini də, papağını da götür");//азербайджанский
        testMethodCounter("Բել դղյակի ձախ ժամն");//армянский
        testMethodCounter("Τάχιστη αλώπηξ βαφής ψημένη γη, δρασκελίζει υπέρ νωθρού κυνός");//греческий
    }


}
