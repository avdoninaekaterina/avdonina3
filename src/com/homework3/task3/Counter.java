package com.homework3.task3;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Counter {

    private final char[] values;

    public Counter(String str) {
        this.values = str.toCharArray();
    }

    private HashMap<Character, Integer> countCharacters() {
        HashMap<Character, Integer> symbolList = new HashMap<>();
        for (char value : values) {
            if (symbolList.containsKey(value)) {
                symbolList.put(value, symbolList.get(value) + 1);
            } else {
                symbolList.put(value, 1);
            }
        }
        return symbolList;
    }

    public LinkedHashMap<Character, Integer> getCountInDescendingOrder() {
        HashMap<Character, Integer> symbolList = countCharacters();
        return sortedMapDecOrder(symbolList);
    }

    private LinkedHashMap<Character, Integer> sortedMapDecOrder(HashMap<Character, Integer> symbolList) {
        return symbolList.entrySet().stream()
                .sorted(Map.Entry.<Character, Integer>comparingByValue().reversed())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
    }

}
