package com.homework3.task1;

import java.util.NoSuchElementException;

public class Main {

    public static void main(String[] args) {
        String[] countries = new String[]{"Germany", "UK", "France", "Russia"};

        try {
            Iterator iterator1 = new Iterator(countries);
            while(iterator1.hasNext()){
                System.out.println(iterator1.next());
            }

            System.out.println("==================");

            Iterator iterator2 = new Iterator(countries);
            iterator2.remove();
            System.out.println("remove Germany");
            iterator2.next();
            iterator2.remove();
            System.out.println("remove France");
            System.out.println("==================");
            iterator2.setIndex(0);
            while(iterator2.hasNext()){
                System.out.println(iterator2.next());
            }
        } catch (NoSuchElementException | IllegalArgumentException exception){
            System.out.println(exception.getMessage());
        }

    }

}
