package com.homework3.task1;

import java.util.NoSuchElementException;

public class Iterator {

    private int count;
    private int index = 0;
    private String[] array;

    public Iterator(String[] array) {
        this.count = array.length;
        this.array = array;
    }

    public void setIndex(int index) {
        if (index >= 0 && index < count) this.index = index;
        else throw new IllegalArgumentException("Index can be positive and less count");
    }

    public boolean hasNext() {
        return index < count;
    }

    public String next() {
        if (hasNext()) {
            return array[index++];
        } else {
            throw new NoSuchElementException("No such element.");
        }
    }

    public void remove() {
        if (array.length > 1) {
            String[] result = new String[array.length - 1];
            System.arraycopy(array, 0, result, 0, index);
            if (index < count - 1) System.arraycopy(array, index + 1, result, index, array.length - index - 1);
            else index--;
            count--;
            array = result;
        } else if (array.length == 1) {
            array = new String[0];
        } else throw new NoSuchElementException("Can't remove item from array");
    }
}
